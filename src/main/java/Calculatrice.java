import java.util.Scanner;

public class Calculatrice {
    public static void main(String[] args) throws Exception {

        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir un nombre :");
        Integer nbr1 = sc.nextInt();
        System.out.println("Veuillez saisir une operation :");
        String ope = sc.next();
        System.out.println("Veuillez saisir un nombre :");
        Integer nbr2 = sc.nextInt();

        System.out.println("Résultat : " + operation(nbr1, ope,nbr2));

    }

    public static double operation(int a, String ope, int b){
        Double result ;
        switch (ope){
            case "+":
                result = add(a,b);
                break;
            case  "-":
                result = sub(a,b);
                break;
            case  "*":
                result = mult(a,b);
                break;
            case "/":
                result = div(a,b);
                break;
            default:
                throw new ArithmeticException("Illegal Operation" + ope);
        }
        return result;
    }

    public static double add(int a, int b){
        return a + b;
    }

    public static double sub(int a, int b){
        return a - b;
    }

    public static double div(int a, int b){
        if (a == 0 || b == 0){
            throw new ArithmeticException();
        }else {
            return a / b;
        }
    }

    public static double mult(int a, int b){
        return a * b;
    }
}
