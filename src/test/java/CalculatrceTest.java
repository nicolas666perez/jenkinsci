import org.junit.Assert;
import org.junit.Test;

public class CalculatrceTest {

    @Test
    public void testMultiply() { Assert.assertTrue(Calculatrice.mult(4,5) == 20.0); }

    @Test
    public void testAdd() {
        Assert.assertTrue(Calculatrice.add(4,5) == 9.0);
    }

    @Test
    public void testSub() {
        Assert.assertTrue(Calculatrice.sub(4,5) == -1.0);
    }

    @Test
    public void testDiv() { Assert.assertTrue(Calculatrice.div(5,5) == 1.0); }
}
